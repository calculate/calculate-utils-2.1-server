#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи значений
#   mode -     режим переменной r-не переназначается из командной строки,
#              w-переназначается из командной строки
#   type     - тип переменной состоит из двух элементов(что это и для чего
#              это)
#   value    - дефолтное значение переменной
#   select   - список допустимых значений переменной
#   official - флаг того, что данная переменная служебная и не отображается
#              при печати списка значений переменных
#   printval - печатное значение переменной(значение выводимое при печати
#              списка значений переменных)

class Data:
    #базовый суффикс LDAP
    ld_base_dn = {}

    #bind суффикс LDAP
    ld_bind_dn = {}

    #пользователь только для чтения
    ld_bind_login = {'value':'proxyuser'}

    #hash пароля для пользователя для чтения
    ld_bind_hash = {}

    #пароль для пользователя для чтения
    ld_bind_pw = {'value':'calculate'}

    #алгоритм шифрования паролей
    ld_encrypt = {'value':'{SSHA}'}

    #имя для базового суффикса LDAP
    ld_base_root = {'value':'calculate'}

    #временный пользователь root для инициализации базы данных
    ld_temp_dn = {}

    #hash пароля временного root
    ld_temp_hash = {}

    #пароль временного пользователя root
    ld_temp_pw = {}

    #DN пользователя root
    ld_admin_dn = {}

    #имя пользователя root для LDAP
    ld_admin_login = {'value':'ldapadmin'}

    #hash пароля root
    ld_admin_hash = {}

    #пароль root
    ld_admin_pw = {}

    #имя samba домена
    sr_samba_domain = {'mode':"w",'value':'Calculate'}

    #netbios имя samba домена
    sr_samba_netbios = {'mode':"w"}

    #Логин LDAP пользователя
    ur_name = {'mode':"w"}

    #Полное имя LDAP пользователя
    ur_fio = {'mode':"w"}

    #ID LDAP пользователя (номер пользователя)
    ur_id = {'mode':"w"}

    #GID LDAP пользователя (номер группы пользователя)
    ur_gid = {'mode':"w"}

    #Домашняя директория LDAP пользователя
    ur_home_path = {'mode':"w"}

    #Оболочка LDAP пользователя
    ur_shell = {'mode':"w"}

    #Хеш пароля LDAP пользователя
    ur_hash = {'mode':"w"}

    #Название группы
    ur_group = {'mode':"w"}

    #ID группы
    ur_group_id = {'mode':"w"}

    #SID группы
    ur_group_sid = {'mode':"w"}

    #Тип группы
    ur_group_type = {'mode':"w"}

    #Произвольный аттрибут группы
    ur_group_attr = {'mode':"w"}

    #Полное имя группы
    ur_group_comment = {'mode':"w"}

    # Видимость пользователя с другого компьютера
    ur_visible = {'mode':"w"}

    #Организация пользователя
    ur_organization = {'mode':'w'}

    #Сигнатура пользователя
    ur_signature = {'mode':'w'}

    #Почтовый адрес пользователя
    ur_mail = {'mode':'w'}

    # имя компьютера с настроенным сервисом Mail
    sr_mail_host = {}

    # шифрование при получении - ''/ssl/tls
    sr_mail_crypt = {'mode':"w",'value':'tls'}
    # порт получения
    sr_mail_port = {'mode':"w",'value':'143'}
    # тип получения - pop3/imap/all
    sr_mail_type = {'mode':"w",'value':'imap'}
    # шифрование отправки - ''/ssl/tls
    sr_mail_send_crypt = {'mode':"w"}
    # порт отправки
    sr_mail_send_port = {'mode':"w",'value':'25'}
    # хост отправки
    sr_mail_send_host = {'mode':"w"}

    # включение хранения истории сообщений mail сервиса
    # по умолчанию выключено
    sr_mail_history = {'mode':"w",
                         'value':'off'}

    sr_mail_history_domain = {'mode':"w"}

    sr_mail_history_path = {'mode':"w",
        'value':"/var/calculate/server-data/samba/share/Mail"}
    # имя компьютера с настроенным сервисом Samba
    sr_samba_host = {}

    # имя компьютера с настроенным сервисом Jabber
    sr_jabber_host = {}

    # имена хостов с которыми работает сервис Jabber
    sr_jabber_hosts = {}
 
    # Текст в ejabberd.cfg - имена хостов с которыми работает сервис
    sr_jabber_hosts_pass = {}

    # Текст в ejabberd.yml - имена хостов с которыми работает сервис
    sr_jabber_hosts_yml = {}

    # jabber id пользователя
    sr_jabber_user_id = {'mode':"w"}

    # имя jabber пользователя (левая часть jabber id)
    sr_jabber_user_name = {'mode':"w"}

    # шифрование при получении - ''/ssl
    sr_jabber_crypt = {'mode':"w",'value':'ssl'}

    # порт jabber сервиса
    sr_jabber_port = {'mode':"w",'value':'5223'}

    # порт jabber сервиса
    sr_jabber_port_starttls = {'mode':"w",'value':'5222'}

    # включение хранения истории сообщений jabber сервиса
    # по умолчанию выключено
    sr_jabber_history = {'mode':"w",
                         'value':'off'}

    #Логин компьютера
    sr_samba_machine_login = {'mode':"w"}

    #ID LDAP компьютера (номер компьютера)
    sr_samba_machine_id = {'mode':"w"}

    #GID LDAP компьютера (номер первичной группы компьютера)
    sr_samba_machine_gid = {'mode':"w"}

    #Название первичной группы компьютера
    sr_samba_machine_group = {'mode':"w",
                              'value':'Domain Computers'}

    #-----------------------------------------------------
    #Все сервисы Unix
    #-----------------------------------------------------
    #Имя для всех сервисов
    ld_services= {'value' : 'Services'}

    #DN всех сервисов
    ld_services_dn =  {}

    #Настроен или нет сервис LDAP
    sr_ldap_set = {'mode':"w",'value':'off'}

    #имя устанавливаемого сервиса
    cl_pass_service = {'mode':"w"}

    #директория куда будут записаны данные удаленных пользователей
    sr_deleted_path = {'mode':"w",
                        'value':'/var/calculate/server-backup/deleted'}

    #-----------------------------------------------------
    # Сервис Unix
    #-----------------------------------------------------
    #DN администратора сервиса Unix (он, же DN сервиса)
    ld_unix_dn = {}

    #имя администратора сервиса Unix
    ld_unix_login = {'value':'Unix'}

    #пароль администратора сервиса Unix
    ld_unix_pw = {}

    #hash пароля администратора сервиса Unix
    ld_unix_hash = {}

    #Настроен или нет сервис Unix
    sr_unix_set = {'mode':"w",
                   'value':'off'}

    #-----------------------------------------------------
    # Сервис Samba
    #-----------------------------------------------------
    #DN администратора сервиса Samba (он, же DN сервиса)
    ld_samba_dn = {}

    #имя администратора сервиса Samba
    ld_samba_login = {'value':'Samba'}

    #пароль администратора сервиса Samba
    ld_samba_pw = {}

    #hash пароля администратора сервиса Samba
    ld_samba_hash = {}

    # Директория настроек пользователя windows
    sr_samba_winprof_path = \
        {'value':'/var/calculate/server-data/samba/profiles/win'}

    # Директория хранения настроек пользователя linux
    sr_samba_linprof_path = \
        {'value':'/var/calculate/server-data/samba/profiles/unix'}

    # Домашняя директория
    sr_samba_home_path = \
        {'value':'/var/calculate/server-data/samba/home'}

    # Директория netlogon
    sr_samba_winlogon_path = \
        {'value':'/var/calculate/server-data/samba/netlogon'}

    # Директория share
    sr_samba_share_path = \
        {'value':'/var/calculate/server-data/samba/share'}

    # Настроен или нет сервис Samba
    sr_samba_set = {'mode':"w",
                    'value':'off'}

    #-----------------------------------------------------
    # Сервис Mail
    #-----------------------------------------------------
    #DN администратора сервиса Mail (он, же DN сервиса)
    ld_mail_dn = {}

    #имя администратора сервиса Mail
    ld_mail_login = {'value':'Mail'}

    #пароль администратора сервиса Mail
    ld_mail_pw = {}

    #hash пароля администратора сервиса Mail
    ld_mail_hash = {}

    # Директория хранения писем
    sr_mail_path = {'value':'/var/calculate/server-data/mail'}

    #Настроен или нет сервис Mail
    sr_mail_set = {'mode':"w",
                   'value':'off'}

    #-----------------------------------------------------
    # Сервис Jabber
    #-----------------------------------------------------
    #DN администратора сервиса Jabber (он, же DN сервиса)
    ld_jabber_dn = {}

    #имя администратора сервиса Jabber
    ld_jabber_login = {'value':'Jabber'}

    #пароль администратора сервиса Jabber
    ld_jabber_pw = {}

    #hash пароля администратора сервиса Jabber
    ld_jabber_hash = {}

    #Настроен или нет сервис Jabber
    sr_jabber_set = {'mode':"w",
                   'value':'off'}

    #-----------------------------------------------------
    # Сервис FTP
    #-----------------------------------------------------
    # имя компьютера с настроенным сервисом FTP
    cl_remote_ftp = {}

    #DN администратора сервиса FTP (он, же DN сервиса)
    ld_ftp_dn = {}

    #имя администратора сервиса FTP
    ld_ftp_login = {'value':'Ftp'}

    #пароль администратора сервиса FTP
    ld_ftp_pw = {}

    #hash пароля администратора сервиса FTP
    ld_ftp_hash = {}

    #Настроен или нет сервис FTP
    sr_ftp_set = {'mode':"w",
                   'value':'off'}

    # Директория для ftp
    sr_ftp_path = {'value':'/var/calculate/server-data/ftp'}

    #-----------------------------------------------------
    # Сервис Proxy
    #-----------------------------------------------------
    # имя компьютера с настроенным сервисом Proxy
    sr_proxy_host = {}

    #DN администратора сервиса Proxy (он, же DN сервиса)
    ld_proxy_dn = {}

    #имя администратора сервиса Proxy
    ld_proxy_login = {'value':'Proxy'}

    #пароль администратора сервиса Proxy
    ld_proxy_pw = {}

    #hash пароля администратора сервиса Proxy
    ld_proxy_hash = {}

    #Настроен или нет сервис Proxy
    sr_proxy_set = {'mode':"w",
                   'value':'off'}

    # порт Proxy сервиса
    sr_proxy_port = {'mode':"w",'value':'8080'}


    ##список накладываемых профилей при установке, наложении профилей
    cl_profile_path = {}

    #путь к директории относительно которой происходит наложение профилей на
    #файлы системы
    cl_root_path = {'value':'/'}

    # имя программы
    cl_name = {'value':'calculate-server'}

    # версия программы
    cl_ver = {'value':'2.1.16'}

    #DN LDAP ветки, в котором будут находится служебные ветки
    ld_ldap_dn = {}
    #имя LDAP ветки 
    ld_ldap_login = {'value':'LDAP'}

    #Репликация
    # имя компьютера c включенной репликацией
    ld_repl_host = {}
    #Включена или нет репликация
    ld_repl_set  = {'mode':"w",'value':'off'}
    # DN ветки репликации
    ld_repl_dn = {}
    # имя ветки репликации
    ld_repl_login = {'value':'Replication'}
    # пароль ветки репликации
    ld_repl_pw = {}
    # хеш пароля ветки репликации
    ld_repl_hash = {}
    #DN ветки хранения последнего посещенного сервера
    ld_repl_worked_dn = {}
    #имя ветки 
    ld_repl_worked_login = {'value':'Worked'}

    # Текст в slapd.conf, в котором находится информация о серверах репликации
    ld_repl_servers_info = {}
    # Текст в slapd.conf, ссылки на серверы репликации
    ld_repl_servers_ref = {}
    # id текущего сервера репликации
    ld_repl_id = {}
    # Доменные имена серверов репликации для веток Unix, Samba
    ld_repl_samba_servers = {}
    # Доменные имена серверов репликации для ветки Unix
    ld_repl_unix_servers = {}
    # Включена или нет репликация для сервиса Unix
    ld_repl_unix_set = {}
    # Доменные имена серверов репликации
    ld_repl_servers = {}
    # id серверов репликации
    ld_repl_ids = {}
    # Включена или нет репликация для сервиса Samba
    ld_repl_samba_set = {}

    # Доступные сети для сервиса Samba
    sr_samba_net_allow = {}
    # Текст в smb.conf - доступные сети
    sr_samba_net_allow_pass = {}
    # Доступные сети для сервиса Mail
    sr_mail_net_allow = {}
    # Текст в main.cf - доступные сети
    sr_mail_net_allow_pass = {}
    # Доступные сети для сервиса Proxy
    sr_proxy_net_allow = {}
    # Текст в squid.conf - доступные сети
    sr_proxy_net_allow_pass = {}
    # Доступные сети для сервиса DNS
    sr_dns_net_allow = {}
    # Текст в named.conf - доступные сети
    sr_dns_net_allow_pass = {}

    #DN ветки хранения реплицируемых алиасов
    ld_repl_mail_dn = {}
    #имя ветки
    ld_repl_mail_login = {'value':'Mail'}

    # Доменные имена серверов репликации для ветки Replication/Mail
    ld_repl_mail_servers = {}
    # Включена или нет репликация для сервиса Mail
    ld_repl_mail_set = {}
    #Настроен или нет сервис Mail как почтовый релей
    sr_mail_relay_set = {'mode':"w",
                         'value':'off'}

    #-----------------------------------------------------
    # Сервис DNS
    #-----------------------------------------------------
    # имя компьютера с настроенным сервисом DNS
    sr_dns_host = {}

    #DN администратора сервиса DNS (он, же DN сервиса)
    ld_dns_dn = {}

    #имя администратора сервиса DNS
    ld_dns_login = {'value':'DNS'}

    #пароль администратора сервиса DNS
    ld_dns_pw = {}

    #hash пароля администратора сервиса DNS
    ld_dns_hash = {}

    #Настроен или нет сервис DNS
    sr_dns_set = {'mode':"w",
                  'value':'off'}

    #-----------------------------------------------------
    # Сервис DHCP
    #-----------------------------------------------------
    #Настроен или нет сервис DHCP
    sr_dhcp_set = {'mode':"w",
                   'value':'off'}
