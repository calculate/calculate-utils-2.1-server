#!/usr/bin/python
import sys
import os
sys.path.insert(0,os.path.abspath('/usr/lib/calculate/calculate-lib/pym'))
sys.path.insert(0,os.path.abspath('/usr/lib/calculate/calculate-server/pym'))
import cl_base
tr = cl_base.lang()
tr.setGlobalDomain('cl_server')
tr.setLanguage(sys.modules[__name__])
import cl_ldap



if __name__ == '__main__':
    ldapObj = cl_ldap.cl_ldap("cl-groupmod")
    obj = cl_ldap.servSamba()

    for def_group_name, def_comment in ((x.name, x.comment) for x in ldapObj.staticGroups.values()):
        print(f"Reseting comment for group: {def_group_name}")
        obj.modGroupSambaServer(def_group_name, {"c" : def_comment})
