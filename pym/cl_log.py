#-*- coding: utf-8 -*-

# Copyright 2008-2010 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import os
import time

class log():
    """Класс для записи в лог"""
    # Директория хранения логов
    logDir = "/var/log/calculate"
    def __init__(self,fileName, addDataTime=True):
        self.logFile = os.path.join(self.logDir,fileName)
        if not os.path.exists(self.logDir):
            os.makedirs(self.logDir)
        self.addDataTime = addDataTime

    def addLog(self, textLog):
        """Добавляет текст в лог файл"""
        if not os.path.exists(self.logFile):
            try:
                fd = os.open(self.logFile, os.O_CREAT,0o600)
                os.close(fd)
            except:
                print("Error creating file %s" % self.logFile)
                return False
        textWrite = textLog
        if not textLog[-1:] == "\n" :
            textWrite = "%s\n" % textLog
        if self.addDataTime:
            textWrite = "%s %s" % (time.strftime("%d/%m/%Y %H:%M:%S",\
                    time.localtime()), textWrite)
        try:
            FD = open (self.logFile, "a")
            FD.write(textWrite)
            FD.close()
        except:
            print("Error writing to file %s" % self.logFile)
            return False
        return True

    def writeError(self, textLog):
        """Добавляет сообщение об ошибке в log"""
        return self.addLog("ERROR: %s" % textLog)
    
    def writeSuccess(self, textLog):
        """Добавляет сообщение об успехе в log"""
        return self.addLog("SUCCESS: %s" % textLog)
