# Calculate format=ldap\
chmod=0640\
chown=root:ldap\
append=replace
include		/etc/openldap/schema/core.schema
include		/etc/openldap/schema/cosine.schema
include		/etc/openldap/schema/nis.schema
include		/etc/openldap/schema/inetorgperson.schema
include		/etc/openldap/schema/misc.schema
#?sr_samba_set==on||cl_pass_service==samba#
include		/etc/openldap/schema/samba.schema
#sr_samba_set#
#?sr_mail_set==on||cl_pass_service==mail#
include		/etc/openldap/schema/mail.schema
#sr_mail_set#
#?sr_dns_set==on||cl_pass_service==dns#
include		/etc/openldap/schema/dnszone.schema
#sr_dns_set#
#?pkg(openldap)<2.4#schemacheck on#pkg#

pidfile		/var/run/openldap/slapd.pid
argsfile	/var/run/openldap/slapd.arg

# Уровень отладочных сообщений
loglevel	0
allow		bind_v2
modulepath	/usr/lib/openldap/openldap

# Доступ к аттрибуту userPassword
access to attrs=userPassword
    by dn="#-ld_admin_dn-#" write
#?sr_samba_set==on||cl_pass_service==samba#
    by dn="#-ld_samba_dn-#" write
#sr_samba_set#
#?sr_unix_set==on||cl_pass_service==unix#
    by dn="#-ld_unix_dn-#" write
#sr_unix_set#
#?sr_mail_set==on||cl_pass_service==mail#
    by dn="#-ld_mail_dn-#" read
#sr_mail_set#
#?sr_jabber_set==on||cl_pass_service==jabber#
    by dn="#-ld_jabber_dn-#" read
#sr_jabber_set#
#?sr_ftp_set==on||cl_pass_service==ftp#
    by dn="#-ld_ftp_dn-#" read
#sr_ftp_set#
#?sr_proxy_set==on||cl_pass_service==proxy#
    by dn="#-ld_proxy_dn-#" read
#sr_proxy_set#
#?sr_dns_set==on||cl_pass_service==dns#
    by dn="#-ld_dns_dn-#" write
#sr_dns_set#
#Доступ к аттрибуту password репликатора
#?pkg(openldap)>2.4&ld_repl_set==on&ld_repl_id!=#
    by dn="#-ld_repl_dn-#" write
#pkg#
    by self read
    by * auth

# Доступ к аттрибутам Samba
#?sr_samba_set==on||cl_pass_service==samba#
access to attrs=sambaLMPassword,sambaNTPassword
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_samba_dn-#" write
#sr_samba_set#
#Доступ к аттрибутам Samba репликатора
#?pkg(openldap)>2.4&ld_repl_set==on&sr_samba_set==on&ld_repl_id!=||pkg(openldap)>2.4&ld_repl_set==on&cl_pass_service==samba&ld_repl_id!=#
    by dn="#-ld_repl_dn-#" write
#pkg#
#?sr_samba_set==on||cl_pass_service==samba#
    by * none
#sr_samba_set#

# Доступ к пользователю только для просмотра
access to dn.base="#-ld_bind_dn-#"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_bind_dn-#" read
    by * none

# Доступ к администратору сервера LDAP
access to dn.base="#-ld_admin_dn-#"
    by dn="#-ld_admin_dn-#" write
    by * none

# Доступ к ветке Samba
#?sr_samba_set==on||cl_pass_service==samba#
access to dn.regex=".*#-ld_samba_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_samba_dn-#" write
    by dn="#-ld_unix_dn-#" write
#sr_samba_set#
#Доступ к ветке Samba репликатора
#?pkg(openldap)>2.4&ld_repl_set==on&sr_samba_set==on&ld_repl_id!=||pkg(openldap)>2.4&ld_repl_set==on&cl_pass_service==samba&ld_repl_id!=#
    by dn="#-ld_repl_dn-#" write
#pkg#
#?sr_samba_set==on||cl_pass_service==samba#
    by dn="#-ld_bind_dn-#" read
    by * none
#sr_samba_set#

# Доступ к ветке Unix
#?sr_unix_set==on||cl_pass_service==unix#
access to dn.regex=".*#-ld_unix_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_samba_dn-#" write
    by dn="#-ld_unix_dn-#" write
# Доступ к ветке Unix репликатора
#?pkg(openldap)>2.4&ld_repl_set==on&sr_unix_set==on&ld_repl_id!=||pkg(openldap)>2.4&ld_repl_set==on&cl_pass_service==unix&ld_repl_id!=#
    by dn="#-ld_repl_dn-#" write
#pkg#
#?sr_unix_set==on||cl_pass_service==unix#
    by dn="#-ld_bind_dn-#" read
    by * none
#sr_unix_set#

# Доступ к ветке Mail
#?sr_mail_set==on||cl_pass_service==mail#
access to dn.regex=".*#-ld_mail_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_mail_dn-#" read
    by * none
#sr_mail_set#

# Доступ к ветке Jabber
#?sr_jabber_set==on||cl_pass_service==jabber#
access to dn.regex=".*#-ld_jabber_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_jabber_dn-#" read
    by * none
#sr_jabber_set#

# Доступ к ветке FTP
#?sr_ftp_set==on||cl_pass_service==ftp#
access to dn.regex=".*#-ld_ftp_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_ftp_dn-#" read
    by * none
#sr_ftp_set#

# Доступ к ветке LDAP
#?pkg(openldap)>2.4&ld_repl_set==on&ld_repl_id!=#
access to dn.regex=".*#-ld_ldap_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_repl_dn-#" read
    by dn="#-ld_bind_dn-#" read
    by dn="#-ld_mail_dn-#" read
    by * none
#pkg#

# Доступ к ветке Proxy
#?sr_proxy_set==on||cl_pass_service==proxy#
access to dn.regex=".*#-ld_proxy_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_proxy_dn-#" read
    by * none
#sr_proxy_set#

# Доступ к ветке DNS
#?sr_dns_set==on||cl_pass_service==dns#
access to dn.regex=".*#-ld_dns_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_dns_dn-#" write
    by * none
#sr_dns_set#

# Доступ к ветке Replication
#?pkg(openldap)>2.4&ld_repl_set==on&ld_repl_id!=#
access to dn.regex=".*#-ld_repl_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn="#-ld_repl_dn-#" write
    by dn="#-ld_bind_dn-#" read
    by dn="#-ld_mail_dn-#" read
    by * none
#pkg#

# Доступ к остальным веткам сервисов
access to dn.regex=".*ou=([^,]+),#-ld_services_dn-#$"
    by dn="#-ld_admin_dn-#" write
    by dn.regex="ou=$1,#-ld_services_dn-#" write
    by * none

# Закрываем доступ к веткам
access to dn.regex=".*,#-ld_services_dn-#"
    by dn="#-ld_admin_dn-#" write
    by * none

# Доступ ко всем аттрибутам
access to *
    by dn="#-ld_admin_dn-#" write
    by self write
    by * read
# Доступ по умолчанию только для чтения
#?pkg(openldap)<2.4#defaultaccess read#pkg#

# Тип базы данных
#?pkg(openldap)<2.6#database	bdb#pkg#
#?pkg(openldap)>2.6#database	mdb#pkg#
suffix		"#-ld_base_dn-#"
# Размер ответа на запрос
sizelimit	unlimited
directory	/var/lib/openldap-data

# Параметры для репликации
#?pkg(openldap)>2.4&ld_repl_set==on&ld_repl_id!=#
rootdn "cn=ldaproot,#-ld_base_dn-#"

#-ld_repl_servers_info-#

#-ld_repl_servers_ref-#

overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100


mirrormode on
serverID #-ld_repl_id-#
#pkg#

index	objectClass	eq
index	cn		pres,sub,eq
index	sn		pres,sub,eq
index	uid		pres,sub,eq
index	uidNumber	eq
index	gidNumber	eq
index	default		sub
